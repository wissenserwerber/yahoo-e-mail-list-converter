; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CMainFrame
LastTemplate=CTreeCtrl
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Yahoo.h"
LastPage=0

ClassCount=6
Class1=CYahooApp
Class2=CYahooDoc
Class3=CYahooView
Class4=CMainFrame

ResourceCount=5
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Class5=CAboutDlg
Resource3=IDD_ABOUTBOX (English (U.S.))
Resource4=IDR_MAINFRAME (English (U.S.))
Class6=CDirTreeCtrl
Resource5=IDD_YAHOO_FORM

[CLS:CYahooApp]
Type=0
HeaderFile=Yahoo.h
ImplementationFile=Yahoo.cpp
Filter=N
LastObject=btnInfo
BaseClass=CWinApp
VirtualFilter=AC

[CLS:CYahooDoc]
Type=0
HeaderFile=YahooDoc.h
ImplementationFile=YahooDoc.cpp
Filter=N
LastObject=ID_APP_ABOUT

[CLS:CYahooView]
Type=0
HeaderFile=YahooView.h
ImplementationFile=YahooView.cpp
Filter=D
BaseClass=CFormView
VirtualFilter=VWC
LastObject=IDC_BTN_PROCESS


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
BaseClass=CFrameWnd
VirtualFilter=fWC
LastObject=btnInfo




[CLS:CAboutDlg]
Type=0
HeaderFile=Yahoo.cpp
ImplementationFile=Yahoo.cpp
Filter=D
LastObject=btnInfo
BaseClass=CDialog
VirtualFilter=dWC

[DLG:IDD_ABOUTBOX]
Type=1
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Class=CAboutDlg

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_MRU_FILE1
Command6=ID_APP_EXIT
Command10=ID_EDIT_PASTE
Command11=ID_APP_ABOUT
CommandCount=11
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command7=ID_EDIT_UNDO
Command8=ID_EDIT_CUT
Command9=ID_EDIT_COPY

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
CommandCount=13
Command4=ID_EDIT_UNDO
Command13=ID_PREV_PANE

[DLG:IDD_YAHOO_FORM]
Type=1
Class=CYahooView
ControlCount=6
Control1=IDC_TREEDIR,SysTreeView32,1350631431
Control2=IDC_path,static,1350696960
Control3=IDC_Result1,static,1342308352
Control4=IDC_Result2,static,1342308352
Control5=IDC_BTN_PROCESS,button,1342242816
Control6=IDC_BTN_EXiT,button,1342242816

[MNU:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=CMainFrame
Command1=ID_APP_EXIT
Command2=btnInfo
CommandCount=2

[ACL:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:IDD_ABOUTBOX (English (U.S.))]
Type=1
Class=CAboutDlg
ControlCount=6
Control1=IDC_STATIC,static,1342308480
Control2=IDC_STATIC,static,1342308352
Control3=IDOK,button,1342373889
Control4=IDC_PROGRAMMER,static,1342308352
Control5=IDC_Info,static,1350701057
Control6=IDC_EXTINFO,edit,1352728580

[CLS:CDirTreeCtrl]
Type=0
HeaderFile=DirTreeCtrl.h
ImplementationFile=DirTreeCtrl.cpp
BaseClass=CTreeCtrl
Filter=W
LastObject=CDirTreeCtrl
VirtualFilter=GWC

