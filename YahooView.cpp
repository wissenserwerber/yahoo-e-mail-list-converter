// YahooView.cpp : implementation of the CYahooView class
//

#include "stdafx.h"
#include "Yahoo.h"

#include "YahooDoc.h"
#include "YahooView.h"
#include <fstream>
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#define WM_DBLCLK WM_USER+5 //User-Defined message for Dbl-Clk
/////////////////////////////////////////////////////////////////////////////
// CYahooView

IMPLEMENT_DYNCREATE(CYahooView, CFormView)

BEGIN_MESSAGE_MAP(CYahooView, CFormView)
//{{AFX_MSG_MAP(CYahooView)
	ON_BN_CLICKED(IDC_BTN_PROCESS, OnBtnProcess)
	ON_BN_CLICKED(IDC_BTN_EXiT, OnBTNEXiT)
	//}}AFX_MSG_MAP
ON_MESSAGE(WM_DBLCLK, OnItemDblClk)
ON_MESSAGE(WM_MYMESSAGE, OnResult)	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CYahooView construction/destruction

CYahooView::CYahooView()
	: CFormView(CYahooView::IDD)
{
	//{{AFX_DATA_INIT(CYahooView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// TODO: add construction code here

}

CYahooView::~CYahooView()
{
}

void CYahooView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CYahooView)
	DDX_Control(pDX, IDC_TREEDIR, m_treeDir);
	//}}AFX_DATA_MAP
}

BOOL CYahooView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CYahooView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	ModifyStyleEx(WS_EX_CLIENTEDGE,0,SWP_FRAMECHANGED | SWP_NOSIZE);
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();
	m_treeDir.Init();
	m_treeDir.SetDblClkFlag(TRUE,WM_DBLCLK);
}

/////////////////////////////////////////////////////////////////////////////
// CYahooView diagnostics

#ifdef _DEBUG
void CYahooView::AssertValid() const
{
	CFormView::AssertValid();
}

void CYahooView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CYahooDoc* CYahooView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CYahooDoc)));
	return (CYahooDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CYahooView message handlers
LRESULT CYahooView::OnItemDblClk(WPARAM wParam,LPARAM lParam)
{
	LPTSTR szTemp1 = (LPTSTR)wParam;
	LPTSTR szTemp2 = (LPTSTR)lParam;

	CString strSelectedPath1 = szTemp1;
	CString strSelectedPath2 = szTemp2;

	GetDlgItem(IDC_path)->SetWindowText(strSelectedPath2);
	return TRUE;
}

LRESULT CYahooView::OnResult(WPARAM wParam,LPARAM lParam)
{
LPTSTR szTemp1 = (LPTSTR)wParam;
LPTSTR szTemp2 = (LPTSTR)lParam;

CString strResult1 = szTemp1;
CString strResult2 = szTemp2;

GetDlgItem(IDC_Result1)->SetWindowText(strResult1);
if(strResult2 != "")GetDlgItem(IDC_path)->SetWindowText(strResult2);
return TRUE;
}

/* Sets member varibale for sending the parent window user 
defined message when a specified file item is double clicked */

void CDirTreeCtrl::SetDblClkFlag(BOOL bFlag, UINT nMsg)
{
	m_bDblClkFlag = bFlag;
	m_nDblClkMsg = nMsg;
}

void CYahooView::OnBtnProcess() 
{
CString rString,rString2,rString3;
this->GetDlgItemText(IDC_path, rString);

//members.txt ist zu oeffnende Datei

ifstream in(rString);
rString2 = rString.Left(rString.ReverseFind('\\')+1);
ofstream out(rString2 + "OE_Email_List.txt");
if (!out) 
{this->GetDlgItem(IDC_Result1)->SetWindowText("File could not be opened!");
out.close();return;}
//else this->GetDlgItem(IDC_Result1)->SetWindowText("");
if (!in)
{this->GetDlgItem(IDC_Result2)->SetWindowText("Error in conversion!");
in.close(); return;}
else this->GetDlgItem(IDC_Result2)->SetWindowText("");


char *first; char *next;
char item[255];
int x = 0;
//error C2664: cannot convert par.2 from 'const char' to 'const char *'
while (in.getline(item, 255, '\n'))
{
first = strtok(item,",");
if (first == NULL) break;
next = strtok(NULL, ",");
if (next == NULL) break;
next = next +1;

while( *(next+x) != NULL)
{
	if (*(next+x) == '\'') *(next+x) = '\0';
x++;
}
out << next << " ; ";
x=0;
}

if (first == NULL || next == NULL) 
{
	GetDlgItem(IDC_Result2)->SetWindowText("This is not a Yahoo E-Mail list file");
	out.close();in.close(); return;
}
out.close();

rString3 = rString.Left(rString.ReverseFind('\\'));
rString3 = rString3.Mid(rString3.ReverseFind('\\'));

rString2 = rString.Left(rString.Find('\\',rString.Find('\\')+1))+ "...\n..." + rString3 + "\\";
GetDlgItem(IDC_Result2)->SetWindowText("In path: " + rString2 + "\nOE_Email_List.txt\nis placed successfully!");
in.close();
}

void CYahooView::OnBTNEXiT() 
{
//exit(0);
::exit(0);	
}
