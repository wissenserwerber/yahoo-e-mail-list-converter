// Yahoo.h : main header file for the YAHOO application
//

#if !defined(AFX_YAHOO_H__FEC00360_51DF_49CD_9745_FED9431F94A4__INCLUDED_)
#define AFX_YAHOO_H__FEC00360_51DF_49CD_9745_FED9431F94A4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CYahooApp:
// See Yahoo.cpp for the implementation of this class
//

class CYahooApp : public CWinApp
{
public:
	CYahooApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CYahooApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CYahooApp)
	afx_msg void OnAppAbout();
	afx_msg void OnbtnInfo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_YAHOO_H__FEC00360_51DF_49CD_9745_FED9431F94A4__INCLUDED_)
