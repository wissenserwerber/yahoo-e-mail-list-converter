// YahooView.h : interface of the CYahooView class
//
/////////////////////////////////////////////////////////////////////////////
#include "DirTreeCtrl.h"
#if !defined(AFX_YAHOOVIEW_H__7F488871_5F60_4D90_AFE6_336A0F4A9469__INCLUDED_)
#define AFX_YAHOOVIEW_H__7F488871_5F60_4D90_AFE6_336A0F4A9469__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CYahooView : public CFormView
{
protected: // create from serialization only
	CYahooView();
	DECLARE_DYNCREATE(CYahooView)

public:
	//{{AFX_DATA(CYahooView)
	enum { IDD = IDD_YAHOO_FORM };
	CDirTreeCtrl	m_treeDir;
	//}}AFX_DATA

// Attributes
public:
	CYahooDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CYahooView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CYahooView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
	LRESULT OnItemDblClk(WPARAM wParam,LPARAM lParam);
	
protected:
	//{{AFX_MSG(CYahooView)
	afx_msg LRESULT OnResult(WPARAM wParam,LPARAM lParam);
	afx_msg void OnBtnProcess();
	afx_msg void OnBTNEXiT();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in YahooView.cpp
inline CYahooDoc* CYahooView::GetDocument()
   { return (CYahooDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_YAHOOVIEW_H__7F488871_5F60_4D90_AFE6_336A0F4A9469__INCLUDED_)
