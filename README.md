# Yahoo! E-Mail List Converter #
Date: July 2006
Yahoo! Groups is a suitable way to communicate between peers. You send just one E-Mail to an address you have chosen and then the mail goes to each member of that group facilitating the address management. However, there are several limitations to this convenience: You cannot send messages having a size greater than 1MB and advertisement lines is appended to each Yahoo groups mail, which isnot always desirable. To overcome these issues you might like to have an e-mail list of all members and to our luck, such a list is provided in the Export link of the Yahoo groups admin page. When you click it, you can get the e-mail list as a .txt file.

But if you ever take a look at that list you would see a list whose format is unrecognizable to mainstream E-Mail client programs such as Outlook Express, because it contains not only E-Mail addresses but also further entries like the Yahoo ID, the name and surname of the member if provided, the E-Mail subscription preferences, and so on...

So what we need to do is to filter out those extra entries from the list, and here comes Yahoo! E-Mail List Converter Applications handy.

![Screenshot:](screenshot.png)

The main features of the application:

--> It employs an integrated file explorer view to easily select the file to be processed. Note that it is more elegant than a File Open Dialog Box popping up on the screen.

--> It checks the file to be converted and makes the conversion solely by using the standard string functions. The output file is named OE_Email_List.txt for clarity.