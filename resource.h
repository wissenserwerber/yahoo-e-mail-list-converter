//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Yahoo.rc
//
#define IDS_STRING1                     1
#define IDD_ABOUTBOX                    100
#define IDD_YAHOO_FORM                  101
#define IDR_MAINFRAME                   128
#define IDR_YAHOOTYPE                   129
#define IDR_DIREXPTYPE                  132
#define IDI_DRIVE                       133
#define IDI_OPEN_FOLDER                 135
#define IDI_FLOPPY_DRIVE                136
#define IDI_CD_DRIVE                    137
#define IDI_CLOSE_FOLDER                138
#define IDI_NETWORK_DRIVE               140
#define IDC_TREE1                       1000
#define IDC_TREEDIR                     1000
#define IDC_path                        1001
#define IDC_PROGRAMMER                  1002
#define IDC_Info                        1003
#define IDC_Result1                     1004
#define IDC_Result2                     1005
#define IDC_BTN_PROCESS                 1006
#define IDC_EXTINFO                     1008
#define IDC_BTN_EXiT                    1009
#define btnInfo                         32771
#define IDS_ERR_DRIVE_READ              61204

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
