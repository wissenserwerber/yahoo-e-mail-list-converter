// DirTreeCtrl.cpp : implementation file
//

#include "stdafx.h"
#include "Yahoo.h"
#include "DirTreeCtrl.h"
#include "stdlib.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#include "io.h"   //required for _finddata, _findnextfile() etc.,

HANDLE g_hUpdateSemph;

CDirTreeCtrl::CDirTreeCtrl()
{
	m_bFileFlag = TRUE;
	m_bDblClkFlag = FALSE;
	//
	m_nDblClkMsg = (UINT)-10;	
	m_nMyMessage = (UINT)-11;

	//Initially set the filter to *.*
	m_strCurFilter = TXT_FILES;

	m_nFolderItemMask = //TVIF_CHILDREN|
						TVIF_HANDLE
						|TVIF_IMAGE
						|TVIF_SELECTEDIMAGE
						|TVIF_STATE
						|TVIF_TEXT;
}

CDirTreeCtrl::~CDirTreeCtrl()
{
}

BEGIN_MESSAGE_MAP(CDirTreeCtrl, CTreeCtrl)
	//{{AFX_MSG_MAP(CDirTreeCtrl)
	ON_NOTIFY_REFLECT(TVN_ITEMEXPANDING, OnItemexpanding)
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/* Performs initilization for Setting ImageList */
void CDirTreeCtrl::Init()
{
	HICON hIcon;
	
	//create image list
	m_ImageList.Create(IMAGE_SIZE,IMAGE_SIZE,ILC_COLOR16,INITIAL,GROW);
	CWinApp* pApp = AfxGetApp();
	
	//Icons related to drives
	hIcon = pApp -> LoadIcon(IDI_DRIVE);
	m_nDriveImage = m_ImageList.Add(hIcon);

	hIcon = pApp -> LoadIcon(IDI_NETWORK_DRIVE);
	m_nNetworkImage = m_ImageList.Add(hIcon);	

	hIcon = pApp -> LoadIcon(IDI_FLOPPY_DRIVE);
	m_nFloppyImage = m_ImageList.Add(hIcon);	

	hIcon = pApp -> LoadIcon(IDI_CD_DRIVE);
	m_nCDImage = m_ImageList.Add(hIcon);	

	//Icons related to folders & files
	hIcon = pApp -> LoadIcon(IDI_CLOSE_FOLDER);
	m_nClsFolderImage = m_ImageList.Add(hIcon);
	//
	hIcon = pApp -> LoadIcon(IDI_OPEN_FOLDER);
	m_nOpenFolderImage = m_ImageList.Add(hIcon);	
	//
	hIcon = pApp -> LoadIcon(IDR_DIREXPTYPE);
	m_nAllFileImage = m_ImageList.Add(hIcon);

TCHAR szAppPath[MAX_PATH] = "";
CString strAppDirectory;

::GetModuleFileName(NULL, szAppPath, sizeof(szAppPath) - 1);
strAppDirectory = szAppPath;
int FirstL = strAppDirectory.GetLength();
strAppDirectory = strAppDirectory.Left(strAppDirectory.ReverseFind('\\'));
int LastL = strAppDirectory.GetLength();
for (int x= LastL+1; x<FirstL ;x++)
szAppPath[x] ='\0';


	g_hUpdateSemph = CreateSemaphore(NULL,1,1,NULL);

	if(!g_hUpdateSemph)
	{
		DWORD dwErr = GetLastError();
	}


	InitDriveInfo();
	m_nMyMessage = WM_MYMESSAGE;
	int retV;
	retV = FirstExpand(szAppPath);
	Expand(GetRootItem(),TVE_EXPAND);
}
/*
* This function fills a specified tree item
*/
BOOL CDirTreeCtrl::FillItem(CString strFindCriteria,HTREEITEM hPresentItem)
{	
	HTREEITEM hTempItem;	
	int nIconImage = m_nAllFileImage;
	CString strPath;
	UINT nFileHandle,nTemp;
	int nFind = strFindCriteria.Find(_T("*"));

	//Retrieve path before *.fileextension (*.*, *.grb etc.,)
	strPath = strFindCriteria.Mid(0,nFind); 
	CString strOrigiPath = strPath;

	_finddata_t data;

	nFileHandle = _tfindfirst((LPCTSTR)strFindCriteria,&data);

	if(nFileHandle == -1)
	{	
		CString strMsg;
		strMsg.LoadString(IDS_ERR_DRIVE_READ);
		CString strTitle;
		strTitle.LoadString(AFX_IDS_APP_TITLE);
		strTitle += ":-Error in Reading Drive";

		MessageBox(strMsg,strTitle,MB_OK|MB_ICONSTOP);

		_findclose(nFileHandle);		
		return FALSE;
	}


	do
	{	
		// if the contents is a folder  
		if(data.attrib & _A_SUBDIR)
		{			
			//Ignore directory with dot "." and ".."
			if( !(_tcscmp(data.name,_T(".")) && _tcscmp(data.name,_T("..")) ) ) 
				continue;

			if(!_tcscmp(data.name,"RECYCLED"))
				continue;

			hTempItem = InsertItem(m_nFolderItemMask,
									data.name,
									m_nClsFolderImage,
									m_nClsFolderImage,
									0,
									TVIS_EXPANDED|TVIS_EXPANDEDONCE|TVIS_SELECTED,
									0,
									hPresentItem,
									TVI_SORT
									);
			InsertItem(DUMMY,hTempItem);
		}
		
	}
	while(!(nTemp = _tfindnext(nFileHandle,&data)));

	_findclose(nFileHandle);

	if(!m_bFileFlag) //no need to fill files
		return TRUE;

	strFindCriteria = strOrigiPath + m_strCurFilter;
	nFileHandle = _tfindfirst((LPCTSTR)strFindCriteria,&data);

	if(nFileHandle == -1)
		return TRUE;

	do
	{
		if(data.attrib & _A_SUBDIR)
			continue;
		
		InsertItem(data.name,m_nAllFileImage,m_nAllFileImage,hPresentItem,TVI_LAST);
		
	}
	while(!(nTemp = _tfindnext(nFileHandle,&data)));

	//close the file handle so that the files/directories can be 
	//accessed from elsewhere
	_findclose(nFileHandle);   
	return TRUE;
}
void CDirTreeCtrl::InitDriveInfo()
{   
	char szBuf[MAX_PATH];
	::GetLogicalDriveStrings(MAX_PATH,szBuf);

	TCHAR szTemp[3];
	CString strTemp;
	int nTemp,nCounter;

	nTemp = nCounter = 0;

	HICON hIcon = AfxGetApp() -> LoadIcon(IDR_MAINFRAME);
	int nRootImage = m_ImageList.Add(hIcon);

	//Attach imagelist to tree control
	SetImageList(&m_ImageList,TVSIL_NORMAL);  //attach image list to tree control

	HTREEITEM hRootItem = InsertItem("My Computer",nRootImage,nRootImage);

	do
	{
		for(;szBuf[nCounter] != NULL; nCounter++,nTemp++)
		szTemp[nTemp] = szBuf[nCounter];
		szTemp[nTemp] = '\0';	
		InsertDriveItem(szTemp);			
		nTemp = 0;
		nCounter++;
	}
	while(szBuf[nCounter] != NULL);
}


void CDirTreeCtrl::InsertDriveItem(CString i_strDriveString)
{
	char szTemp[9];
	strcpy(szTemp,i_strDriveString);
	int nType = ::GetDriveType(szTemp);
	HTREEITEM hRootItem = GetRootItem();
	HTREEITEM hDriveItem;
	CString strTemp;
	switch(nType)
	{
	case DRIVE_REMOVABLE:
		hDriveItem = InsertItem(m_nFolderItemMask,
								szTemp,
								m_nFloppyImage,
								m_nFloppyImage,
								0,
								TVIS_EXPANDED|TVIS_EXPANDEDONCE|TVIS_SELECTED,
								0,
								hRootItem,
								TVI_SORT
								);
		InsertItem(DUMMY,hDriveItem);
		strTemp = szTemp;
		m_astrDriveStrings.Add(strTemp);
		LookForChanges(strTemp);
		break;
	case DRIVE_FIXED :
		hDriveItem = InsertItem(m_nFolderItemMask,
								szTemp,
								m_nDriveImage,
								m_nDriveImage,
								0,
								TVIS_EXPANDED|TVIS_EXPANDEDONCE|TVIS_SELECTED,
								0,
								hRootItem,
								TVI_SORT
								);
		InsertItem(DUMMY,hDriveItem);
		strTemp = szTemp;
		m_astrDriveStrings.Add(strTemp);
		LookForChanges(strTemp);
		break;
	case DRIVE_REMOTE :
		hDriveItem = InsertItem(m_nFolderItemMask,
								szTemp,
								m_nNetworkImage,
								m_nNetworkImage,
								0,
								TVIS_EXPANDED|TVIS_EXPANDEDONCE|TVIS_SELECTED,
								0,
								hRootItem,
								TVI_SORT
								);

		InsertItem(DUMMY,hDriveItem);
		strTemp = szTemp;
		m_astrDriveStrings.Add(strTemp);		
		break;
	case DRIVE_CDROM:
		hDriveItem = InsertItem(m_nFolderItemMask,
								szTemp,
								m_nCDImage,
								m_nCDImage,
								0,
								TVIS_EXPANDED|TVIS_EXPANDEDONCE|TVIS_SELECTED,
								0,
								hRootItem,
								TVI_SORT
								);

		InsertItem(DUMMY,hDriveItem);
		strTemp = szTemp;
		m_astrDriveStrings.Add(strTemp);		
		break;
	default:
		hDriveItem = InsertItem(m_nFolderItemMask,
								szTemp,
								m_nDriveImage,
								m_nDriveImage,
								0,
								TVIS_EXPANDED|TVIS_EXPANDEDONCE|TVIS_SELECTED,
								0,
								hRootItem,
								TVI_SORT
								);
		InsertItem(DUMMY,hDriveItem);
		strTemp = szTemp;
		m_astrDriveStrings.Add(strTemp);
		break;
	}
}

/*
* This function returns the full directory path of a given item
*/
CString CDirTreeCtrl::GetFullPath(CString strPath, HTREEITEM hItem)
{
	CString strParentText;	
	HTREEITEM hParentItem;

	if(hItem == GetRootItem())
		return strPath;
	
	hParentItem	= GetParentItem(hItem);

	if(hParentItem == GetRootItem())
		return strPath;

	strParentText = GetItemText(hParentItem);
	if(IsDriveItem(hParentItem))
		strPath = (strParentText + strPath);
	else
		strPath = (strParentText + PATH_SEPERATOR + strPath);

	strPath = GetFullPath(strPath,hParentItem);	
CString strSpecial= strPath;
	HWND hParentWnd = ::GetParent(this->m_hWnd);
	LPARAM lParam = (LPARAM)((LPCTSTR) strSpecial);
	return strPath;
}
/*This is message handler called when the item is expanding*/
void CDirTreeCtrl::OnItemexpanding(NMHDR* pNMHDR, LRESULT* pResult) 
{NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	//
	CString strFindCriteria = GetItemText((pNMTreeView -> itemNew).hItem);			
	UINT nMask = GetItemState((pNMTreeView -> itemNew).hItem,TVIS_EXPANDEDONCE);
	
	if(!(nMask & TVIS_EXPANDEDONCE))  //the item is first time expanding
	{	
		if((pNMTreeView -> itemNew).hItem != GetRootItem())
		{
			strFindCriteria = GetFullPath(strFindCriteria,(pNMTreeView -> itemNew).hItem);	
			strFindCriteria += PATH_SEPERATOR;// Als auch eine spezielle Character??
			strFindCriteria += ALL_FILES;			

			//get DUMMY child item		
			HTREEITEM hItemTemp = GetChildItem((pNMTreeView -> itemNew).hItem);
			BeginWaitCursor();
			BOOL bRetVal = FillItem(strFindCriteria, (pNMTreeView -> itemNew).hItem);

			if(!bRetVal)
			{
				*pResult = 1; //return TRUE to prevent item from expanding
				return;
			}
			//delete DUMMY item	
			if(hItemTemp != NULL)
			      DeleteItem(hItemTemp);
			EndWaitCursor();			
		}
	}
	if(IsDriveItem((pNMTreeView -> itemNew).hItem))
	{
		*pResult = 0;
		return;
	}
	if(pNMTreeView -> action == TVE_COLLAPSE)
		  SetItemImage((pNMTreeView -> itemNew).hItem,m_nClsFolderImage,m_nClsFolderImage);
	//
	if(pNMTreeView -> action == TVE_EXPAND)
		SetItemImage((pNMTreeView -> itemNew).hItem,m_nOpenFolderImage,m_nOpenFolderImage);

	*pResult = 0;

}

BOOL CDirTreeCtrl::IsDriveItem(HTREEITEM i_hItem)
{
	if(GetRootItem() == i_hItem)
		return TRUE;
	
	if(GetParentItem(i_hItem) != GetRootItem())
		return FALSE;
	else
		return TRUE;
}

void CDirTreeCtrl::UpdateDriveInfo()
{
	char szBuf[MAX_PATH];
	::GetLogicalDriveStrings(MAX_PATH,szBuf);

	TCHAR szTemp[3];
	CString strTemp;
	int nTemp,nCounter,nExistDriveCnt,nNewDriveCnt;

	nTemp = nCounter = 0;

	CStringArray astrTempDriveStrings;

	do
	{
		for(;szBuf[nCounter] != NULL; nCounter++,nTemp++)
			szTemp[nTemp] = szBuf[nCounter];
		//
		szTemp[nTemp] = '\0';

		strTemp = szTemp;
		astrTempDriveStrings.Add(strTemp);
		nTemp = 0;
		nCounter++;
	}
	while(szBuf[nCounter] != NULL);
	
	nExistDriveCnt = m_astrDriveStrings.GetSize();
	nNewDriveCnt = astrTempDriveStrings.GetSize();

	//if some drive is removed/unmapped update existing list
	for(int nOuter=0; nOuter<nExistDriveCnt; nOuter++)
	{
		strTemp = m_astrDriveStrings.GetAt(nOuter);
		for(int nInner=0; nInner<nNewDriveCnt; nInner++)
		{			
			if(!strTemp.CompareNoCase(astrTempDriveStrings.GetAt(nInner)))
			{
				break;						
			}
		}

		if(nInner >= nNewDriveCnt)  //string not found
		{
			DeleteDriveItem(strTemp);
			m_astrDriveStrings.RemoveAt(nOuter);
			nOuter--;
			nExistDriveCnt = m_astrDriveStrings.GetSize();
		}
		
	}

	//If Drive not already there add it 
	for(nOuter=0; nOuter<nNewDriveCnt; nOuter++)
	{
		strTemp = astrTempDriveStrings.GetAt(nOuter);
		for(int nInner=0; nInner<nExistDriveCnt; nInner++)
		{			
			if(!strTemp.CompareNoCase(m_astrDriveStrings.GetAt(nInner)))
			{
				break;						
			}
		}
		
		if(nInner >= nExistDriveCnt) //string not found
		{
			InsertDriveItem(strTemp);
			m_astrDriveStrings.Add(strTemp);
			nExistDriveCnt = m_astrDriveStrings.GetSize();
		}
	}
}

void CDirTreeCtrl::DeleteDriveItem(CString i_strDriveName)
{	HTREEITEM hDriveItem,hRootItem;

	hRootItem = GetRootItem();

	hDriveItem = GetChildItem(hRootItem);

	CString strTemp;

	while(hDriveItem)
	{
		strTemp = GetItemText(hDriveItem);

		if(!strTemp.CompareNoCase(i_strDriveName))  //drive found
		{
			DeleteItem(hDriveItem);
			break;
		}
		hDriveItem = GetNextSiblingItem(hDriveItem);
	}
}

void CDirTreeCtrl::OnLButtonDblClk(UINT nFlags, CPoint point) 
{ 
UINT nHitFlags = TVHT_ONITEM;	

HTREEITEM hSelectedItem = HitTest(point,&nHitFlags);
	if(m_bDblClkFlag && hSelectedItem)
	{	
		CString strPath = GetItemText(hSelectedItem);
		strPath = GetFullPath(strPath,hSelectedItem);
		m_strSelectedPath = strPath;
		HWND hParentWnd =::GetParent(this -> m_hWnd);
		LPARAM lParam = (LPARAM)((LPCTSTR)(m_strSelectedPath));
		WPARAM wParam = (WPARAM)((LPCTSTR)(m_strSelectedPath));
		::PostMessage(hParentWnd,m_nDblClkMsg,wParam,lParam);
	}
	
	CTreeCtrl::OnLButtonDblClk(nFlags, point);
}

void CDirTreeCtrl::MyResult(CString str1, CString str2)
{
	m_strPath1= str1;m_strPath2= str2; //Diese sollen global sein!!!
	HWND hParentWnd =::GetParent(this -> m_hWnd);
	WPARAM wParam = (WPARAM)((LPCTSTR)(m_strPath1));
	LPARAM lParam = (LPARAM)((LPCTSTR)(m_strPath2));
	::PostMessage(hParentWnd,m_nMyMessage, wParam,lParam);
}

int CDirTreeCtrl::FirstExpand(TCHAR* strApp)
{
CDirTreeCtrl* pCtrl = this;
CStringArray Splitted;

	CString strTemp;
	int i=0;
	int nTemp = 0;
	int nCounter = 0;
	TCHAR szTemp[1000];
	do
	{
	for (; strApp[nCounter] != '\\'; nTemp++, nCounter++)
	szTemp[nTemp] = strApp[nCounter];
	szTemp[nTemp]='\0';
	
	strTemp = szTemp;
	
	 Splitted.Add(strTemp);
	nTemp=0;
	nCounter++;
	}
	while (strApp[nCounter] != '\0');
Splitted[0]+="\\";
bool fFlag = false;
HTREEITEM hCurrent = pCtrl->GetRootItem(); //GetNextItem(TVI_ROOT, TVGN_NEXT);
hCurrent = GetChildItem(hCurrent);
while (hCurrent != NULL)
{  TVITEM item;
   TCHAR szText[1024];
   item.hItem = hCurrent;
   item.mask = TVIF_TEXT | TVIF_HANDLE;
   item.pszText = szText;
   item.cchTextMax = 1024;
   BOOL bWorked = pCtrl->GetItem(&item);
   if (i< Splitted.GetSize())
   {
   if (bWorked && !_tcscmp(item.pszText, Splitted[i]))
		{	pCtrl->Expand(item.hItem,TVE_EXPAND);
			pCtrl->Select(item.hItem,TVGN_CARET);
			hCurrent = GetChildItem(hCurrent);
			fFlag= true;
		i++; }
   }
   if (!fFlag) hCurrent = pCtrl->GetNextItem(hCurrent, TVGN_NEXT);
   fFlag = false;
}
pCtrl->ScrollWindow(4,5,NULL,NULL);
pCtrl->SetScrollRange(SB_HORZ,1,100,TRUE);
pCtrl->SetScrollPos(SB_HORZ,90,TRUE);
hCurrent = pCtrl->GetSelectedItem();

	{	
		CString strPath = GetItemText(hCurrent);
		strPath = GetFullPath(strPath,hCurrent);
		m_strSelectedPath = strPath;
		HWND hParentWnd = ::GetParent(this -> m_hWnd);
		LPARAM lParam = (LPARAM)((LPCTSTR)(m_strSelectedPath));
		WPARAM wParam = (WPARAM)((LPCTSTR)(m_strSelectedPath));
::PostMessage(hParentWnd,m_nMyMessage,0,lParam);
	}
return Splitted.GetSize();
}


void CDirTreeCtrl::LookForChanges(CString i_strDrive)
{
	DWORD dwThreadId;

	WaitForSingleObject(g_hUpdateSemph,INFINITE);
	m_strDrive = i_strDrive;
	
	HANDLE hThread = CreateThread(NULL,
								  0,
								  (LPTHREAD_START_ROUTINE)CDirTreeCtrl::TrackChanges,
								  this,
								  0,
								  &dwThreadId
								 );
	CloseHandle(hThread);
}

void CDirTreeCtrl::TrackChanges(LPVOID pParam)
{
	HANDLE hDir = NULL;
	
	CDirTreeCtrl* pTree = (CDirTreeCtrl*)pParam;
	
	CString strDrive = pTree->m_strDrive;

	hDir = CreateFile( strDrive,
					   FILE_LIST_DIRECTORY,
					   FILE_SHARE_READ|FILE_SHARE_WRITE,
					   NULL,
					   OPEN_EXISTING,
					   FILE_FLAG_BACKUP_SEMANTICS,
					   NULL
					 );
 
	long dwPrevCnt;
	ReleaseSemaphore(g_hUpdateSemph,1,&dwPrevCnt);	
	
	if(hDir == INVALID_HANDLE_VALUE)
		return;


	char szBuff[MAX_PATH*5];
	DWORD dwBytesWritten,dwErr;

	while(1)
	{
		BOOL bRes = ReadDirectoryChangesW(hDir,
										  szBuff,
										  sizeof(szBuff),
										  TRUE,
										  FILE_NOTIFY_CHANGE_FILE_NAME
										  |FILE_NOTIFY_CHANGE_DIR_NAME,
										  &dwBytesWritten,
										  NULL,
										  NULL				
										 );

		if(!bRes)
		{
			dwErr = GetLastError();
			break;
			//return;
		}

		else
		{			
			FILE_NOTIFY_INFORMATION* pFileNotify = NULL;

			szBuff[dwBytesWritten] = '\0';

			pFileNotify = (FILE_NOTIFY_INFORMATION*)&szBuff;

			FILE_NOTIFY_INFORMATION* pTempFileNotify = pFileNotify;

			if(!pTempFileNotify)
			{
				continue;
			}
			
			DWORD dwFileNameLen;
			CString strTemp;
			DWORD nTemp,nIndex;
			
			BOOL bFlag = TRUE;			
			long dwPrevCnt;
			
			while(bFlag)
			{
				if(pTempFileNotify->NextEntryOffset == 0)
					bFlag = FALSE;

				dwFileNameLen = pTempFileNotify->FileNameLength;

				char* pszBuff = new char[dwFileNameLen/2 + 1];
				if(!pszBuff)
					break;

				char* pTemp = (char*)pTempFileNotify->FileName;
				
				nTemp = nIndex = 0;

				for(; nTemp<dwFileNameLen;nIndex++)
				{
					pszBuff[nIndex] = pTemp[nTemp];
					nTemp += 2;
				}

				pszBuff[nIndex] = '\0';

				if(!pszBuff)
					delete pszBuff;
				
				strTemp = pszBuff;

				strTemp = (strDrive+strTemp);

				WaitForSingleObject(g_hUpdateSemph,INFINITE);
				if(!pTree -> Update(strTemp,pTempFileNotify->Action))
				{
				}				
				ReleaseSemaphore(g_hUpdateSemph,1,&dwPrevCnt);

				pTempFileNotify 
					= (FILE_NOTIFY_INFORMATION*)
					  ((PBYTE)pTempFileNotify + pTempFileNotify->NextEntryOffset);
			}
		}
	}

	CloseHandle(hDir);	
}

BOOL CDirTreeCtrl::Update(CString strCurPath, int nAction)
{
	static HTREEITEM hRenameOld = NULL;
	BOOL bRetFlag = TRUE;
	int nPos = -1;
	HTREEITEM hItem = NULL;
	UINT nMask;
	CString strPath,strItem;

	switch(nAction)
	{
	case FILE_ACTION_ADDED:
		nPos = strCurPath.ReverseFind('\\');
		if(nPos == -1)
			break;
				
		strPath = strCurPath.Mid(0,nPos+1);
		hItem = GetItemFrmPath(strPath);
		strItem = strCurPath.Mid(nPos+1);
		
		if(hItem)
		{
			nMask = GetItemState(hItem,TVIS_EXPANDED);
			
			if(!(nMask & TVIS_EXPANDED)) //not yet expanded
			{
				DeleteAllItems(hItem);
				InsertItem(DUMMY,hItem);
				SetItemState(hItem,0,TVIS_EXPANDEDONCE);
				break;
			}
			
			CString strTemp = GetItemText(hItem);
			nPos = strCurPath.Find('.');

			if(nPos == -1)  //. not found, should be a directroy
			{
				HTREEITEM hItemTemp = NULL;
				hItemTemp = InsertItem(m_nFolderItemMask,
										strItem,
										m_nClsFolderImage,
										m_nClsFolderImage,
										0,
										TVIS_EXPANDED|TVIS_EXPANDEDONCE|TVIS_SELECTED,
										0,
										hItem,
										TVI_SORT
										);

				InsertItem(DUMMY,hItemTemp);

			}
			else if(m_bFileFlag)
			{
				InsertItem(strItem,m_nAllFileImage,m_nAllFileImage,hItem);
			}
		}
		bRetFlag = TRUE;
		break;

	case FILE_ACTION_REMOVED:
		hItem = GetItemFrmPath(strCurPath);
		if(hItem)
		{
			bRetFlag = TRUE;
			DeleteItem(hItem);
		}
		break;

	case FILE_ACTION_RENAMED_OLD_NAME:
		
		hRenameOld = GetItemFrmPath(strCurPath);
		break;

	case FILE_ACTION_RENAMED_NEW_NAME:
		nPos = strCurPath.ReverseFind('\\');
		if(nPos != -1 && hRenameOld)
		{
			CString strTemp = strCurPath.Mid(nPos+1);
			SetItemText(hRenameOld,strTemp);
			bRetFlag = TRUE;
		}
		break;

	default:
		break;
	}

	return bRetFlag;
}

HTREEITEM CDirTreeCtrl::GetItemFrmPath(CString i_strPath)
{
	int nFind = i_strPath.Find('\\');
	if(nFind == -1)
		return NULL;

	HTREEITEM hRetItem = NULL;

	HTREEITEM hRoot = GetDriveItem(i_strPath.Mid(0,nFind+1));
	if(!hRoot)
		return NULL;
	//	

	hRetItem = ExpandPathToItem(i_strPath,hRoot);

	if(!IsDirectory(hRetItem))  //item is file image
	{
		if(!m_bFileFlag)
			return NULL;
	}

	return hRetItem;	
}

HTREEITEM CDirTreeCtrl::GetDriveItem(CString i_strPath)
{
	int nPos = i_strPath.Find('\\');

	if(nPos == -1)
		return NULL;

	HTREEITEM hDriveItem,hRootItem;
	CString strDrive = i_strPath.Mid(0,nPos+1);

	hRootItem = GetRootItem();

	hDriveItem = GetChildItem(hRootItem);

	while(hDriveItem)
	{
		if(!strDrive.CompareNoCase(GetItemText(hDriveItem)))
		{
			return hDriveItem;
		}

		hDriveItem = GetNextSiblingItem(hDriveItem);
	}

	return NULL;
}

HTREEITEM CDirTreeCtrl::ExpandPathToItem(CString i_strPath,HTREEITEM i_hItem)
{
	CString strPath,strItem;
	if(!i_hItem)
		return NULL;

	HTREEITEM hRetItem = NULL;
	HTREEITEM hChildItem = NULL;

	hChildItem = GetChildItem(i_hItem);
	
	int nFind = i_strPath.Find('\\');
	strPath =  i_strPath.Mid(nFind+1);  //extract path excluding the first seperator
	//
	strPath += PATH_SEPERATOR;
	nFind = strPath.Find('\\');
	strItem = strPath.Mid(0,nFind);

	if(strItem.IsEmpty())
		return i_hItem;
	//
	CString strTemp;	
	do
	{
		strTemp = GetItemText(hChildItem);
		
		if(!strItem.CompareNoCase(strTemp))
			hRetItem = ExpandPathToItem(strPath,hChildItem);
		
	}
	while((hChildItem = GetNextSiblingItem(hChildItem)) != NULL);
	
	return hRetItem;
}

BOOL CDirTreeCtrl::IsDirectory(HTREEITEM hItem)
{
	if(hItem == GetRootItem())
		return FALSE;
	
	int nImage;
	GetItemImage(hItem,nImage,nImage);
	
	if(nImage == m_nAllFileImage)
		return FALSE;
	else
		return TRUE;
}

void CDirTreeCtrl::DeleteAllItems(HTREEITEM hItem)
{
	HTREEITEM hChildItem = GetChildItem(hItem);
	HTREEITEM hTempItem;
	while(hChildItem != NULL)
	{
		hTempItem = hChildItem;
		hChildItem = GetNextSiblingItem(hChildItem);
		DeleteItem(hTempItem);
	}

}
