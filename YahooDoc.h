// YahooDoc.h : interface of the CYahooDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_YAHOODOC_H__DADFD6F5_E331_45E6_B47E_8D3C5D41F39A__INCLUDED_)
#define AFX_YAHOODOC_H__DADFD6F5_E331_45E6_B47E_8D3C5D41F39A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CYahooDoc : public CDocument
{
protected: // create from serialization only
	CYahooDoc();
	DECLARE_DYNCREATE(CYahooDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CYahooDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CYahooDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CYahooDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_YAHOODOC_H__DADFD6F5_E331_45E6_B47E_8D3C5D41F39A__INCLUDED_)
