// YahooDoc.cpp : implementation of the CYahooDoc class
//

#include "stdafx.h"
#include "Yahoo.h"

#include "YahooDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CYahooDoc

IMPLEMENT_DYNCREATE(CYahooDoc, CDocument)

BEGIN_MESSAGE_MAP(CYahooDoc, CDocument)
	//{{AFX_MSG_MAP(CYahooDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CYahooDoc construction/destruction

CYahooDoc::CYahooDoc()
{
	// TODO: add one-time construction code here

}

CYahooDoc::~CYahooDoc()
{
}

BOOL CYahooDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CYahooDoc serialization

void CYahooDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CYahooDoc diagnostics

#ifdef _DEBUG
void CYahooDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CYahooDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CYahooDoc commands
