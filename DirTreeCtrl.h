#if !defined(AFX_DIRTREECTRL_H__645A81BE_6CBE_4182_9617_2A3570214267__INCLUDED_)
#define AFX_DIRTREECTRL_H__645A81BE_6CBE_4182_9617_2A3570214267__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DirTreeCtrl.h : header file
//

#define IMAGE_SIZE			16
#define ALL_FILES			_T("*.*")
#define TXT_FILES			_T("*.txt")
#define NONE				_T("")
#define PATH_SEPERATOR		_T("\\")
#define INITIAL				5
#define GROW				7
#define DUMMY				_T("AAAAA")  //all As given so that it will be first item when sorted and will be delted
#define WM_MYMESSAGE WM_USER+6 //User-Defined message
#include <afxtempl.h>


/////////////////////////////////////////////////////////////////////////////
// CDirTreeCtrl
/*
*<<FNH>>********************************************************************************
*
* Class name	: CDirTreeCtrl
* Function name	: CDirTreeCtrl()
* Return value	: none
*
* Description	: Constructor; performs default initializations
*>>HNF<<********************************************************************************
*/

class CDirTreeCtrl : public CTreeCtrl
{
// Construction
public:
	CDirTreeCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDirTreeCtrl)
	//}}AFX_VIRTUAL

// Implementation
		void Init();
		CString GetFullPath(CString strPath , HTREEITEM hItem); 
		void DeleteDriveItem(CString i_strDriveName);
		void SetDblClkFlag(BOOL bFlag, UINT nMsg);
		void MyResult(CString str1,CString str2);
		int FirstExpand(TCHAR* strApp);
		static void TrackChanges(LPVOID pParam);
		BOOL Update(CString strPath,int nAction);
		inline BOOL IsDirectory(HTREEITEM hItem);	//checks whether the item is directory or not
		CString m_strDrive;
public:
	virtual ~CDirTreeCtrl();

	// Generated message map functions
protected:
	//{{AFX_MSG(CDirTreeCtrl)
	afx_msg void OnItemexpanding(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	void InsertDriveItem(CString i_strDriveString);
	BOOL IsDriveItem(HTREEITEM i_hItem);
	void UpdateDriveInfo();
	UINT m_nFolderItemMask;
	CString m_strCurFilter;
	CImageList m_ImageList;
	CStringArray m_astrDriveStrings;
	void LookForChanges(CString i_strDrive);
	HTREEITEM GetItemFrmPath(CString i_strPath);
	HTREEITEM GetDriveItem(CString i_strPath);
	HTREEITEM ExpandPathToItem(CString i_strPath,HTREEITEM i_hItem);
		//stores images of different items (drive, folder etc.,)
	int m_nDriveImage,m_nCDImage,m_nFloppyImage,m_nNetworkImage;
	int m_nClsFolderImage,m_nOpenFolderImage,m_nAllFileImage;

	//User defined message to process double click
	UINT m_nDblClkMsg;
	UINT m_nMyMessage;
	//CString c;
	
	//to store currently expanded path
	CString m_strSelectedPath;
	CString m_strPath1;
	CString m_strPath2;

	//Enable/disable Double click option
	BOOL m_bDblClkFlag;
   
	//Enable/Disable "Show only Direcotires" flag
	BOOL m_bFileFlag;	
	
	//deletes all items of the given parent
	void DeleteAllItems(HTREEITEM hItem);
	
	//fills all "file items" of the given parent
	BOOL FillFileItems(HTREEITEM hItem);

	//sets directory state depending upon closed/open etc.,
	void SetSubDirState(HTREEITEM hItem);

	//deletes all "file items" of the given parent
	void DeleteFileItems(HTREEITEM hItem);
	
	//fills the entire Directory structre of the given item
	BOOL FillItem(CString strFindCriteria, HTREEITEM hPresentItem);

	//Reads all existing drives and Inserts with drive images
	void InitDriveInfo();

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIRTREECTRL_H__645A81BE_6CBE_4182_9617_2A3570214267__INCLUDED_)